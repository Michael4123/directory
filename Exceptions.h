#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

class Exception
{
protected:
	const char* error;
public:
	virtual const char* error_cause() = 0;
};

class NullPointerException : public Exception
{
public:
	NullPointerException()	{error = "NullPointerException";	}
	const char* error_cause()	{return error;	}
};

class SyscallException : public Exception
{
public:
	SyscallException()	{error = "SyscallFailedException";	}
	const char* error_cause()	{return error;	}
};

class BadInputException : public Exception
{
public:
	BadInputException()	{error = "BadInputException";	}
	const char* error_cause()	{return error;	}
};
#endif

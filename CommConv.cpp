#include "CommConv.h"

void AuthenticateProtocol::recv(int sock)	
{
	// user and pass are pointers to null, or at least
	// not used
	int len = IntegerServerData::get_int(sock);
	if (len > SIZE || len <= 0) {
		throw new BadInputException();
	}
	user = read_min(len, sock);

	len = IntegerServerData::get_int(sock);
	if (len > SIZE || len <= 0) {
		throw new BadInputException();
	}
	pass = read_min(len, sock);
}


void AddRouterProtocol::recv(int sock)
{
	ipv4 = IntegerServerData::get_int(sock);
	port = IntegerServerData::get_int(sock);

	short len = ShortServerData::get_short(sock);

	if (len <= 0)	{
		throw new BadInputException();
	}
	pubk = read_min(sock, len);
}

void send_server(int sock, ServerData** data, int len, bool more_data)
{
	TBConv bval;
	bval.val = 0;
	for (int i = 0; i < len; ++i)	
		bval.val += data[i]->get_len();
	

	if (more_data)	{
		bval.val |= (1 << 15);
	}
	cout << bval.val << endl;
	write(sock, bval.bin, 2);
	
	for (int i = 0; i < len; ++i)	{
		data[i]->send_data(sock);
	}
}


#include <iostream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <queue>
#include <signal.h>
#include <sqlite3.h>
#include <netinet/in.h>
#include <vector>
#include <ctime>
#include <errno.h>

#include <sys/stat.h>

#include "MyParser.h"
#include <openssl/rand.h>
#include <openssl/sha.h>

#include "util.h"
#include "CommConv.h"

#define MAX_SIZE 4096

#define HASH_LEN 32
#define HASH_TIME 2 

#define ID_LEN 12 

#define PUBK_DIR "public-keys"

extern Debug debug;

using namespace std;

class user_info
{
public:
	user_info(){
		username = NULL;
	}
	~user_info()	{
		if (username != NULL) delete username;
	}
	void set_username(unsigned char* st)	{
		int len = strlen((char*) st);
		username = new unsigned char[len + 1];
		for (int i = 0; i < len; ++i) username[i] = st[i];
		username[len] = 0;
	}

	void set_hash(unsigned char* hash)	{
		for (int i = 0; i < HASH_LEN; ++i) this->hash[i] = hash[i];
	}

	void set_salt(unsigned char* salt)	{
		for (int i = 0; i < HASH_LEN; ++i) this->salt[i] = salt[i];
	}

	unsigned char* username;
	unsigned char hash[HASH_LEN];
	unsigned char salt[HASH_LEN];
};

struct ThreadParams
{
	int idx;
	int cl_sock;
};

enum CODES {LOGIN = 1, ADD_ROUTER, ADD_ADMIN, GET_ROUTERS, REMOVE_ROUTER, GET_ROUTERS_CLIENT, GET_ROUTER_PK};
enum LOGIN_CODES {AUTH_OK = 1, USER_NEX, WRNG_PAS};

const int TODO_BOOM = 1;
int get_user(sqlite3*, user_info*);
int add_to_table(char* username, char* password);
void hex_dump(void* h_str, int len);
void compute_hash_salt(unsigned char* , int , unsigned char* , unsigned char* , int = 2);
int _add_to_table(char* , char* , char* );
int check_user_login(char*, char*);
int _add_router(int, int, char*, char*, char*);
int add_router(int, int, char*);
void app_main(int);
void add_router_sock(int);
void check_login(int);

void _send_routers(sqlite3*, const char*, int);
int get_routers_admin(sqlite3*, int);
int get_routers_client(sqlite3*, int);
void remove_router(int);
int _remove_router(int , int );
void assert_dir();
void add_pubk_file(const char*, const char*, int) ;
void get_router_pubk(int);
string _get_router_pubk(char*);
int add_admin(int);

/***
Note - for now, I decided *not* to add a function
that checks whether the credentials are ok because it
just seems too dangerous 
****/

void* looper(void*);

pthread_mutex_t vec_mtx;
pthread_mutex_t db_mtx;

sqlite3* db;
vector<pthread_t>* vec;
volatile sig_atomic_t signal_sent = 0;

void exit_error(const char* msg)	{
	perror(msg);
	terminate();
}

void sigint_han(int sig)	{
	signal_sent = 1;
}
void* forward(void*);

const char* DATABASE_PATH = "sqlite3.db";
int main(int argc, char** argv)
{
	if (argc < 2)	{
		cout << "Port was not specified\n";
		return 1;
	}

	signal(SIGINT, sigint_han);

	sqlite3_stmt* stmt;
	char* szErrorMsg = NULL;
	int rc = sqlite3_open(DATABASE_PATH, &db);
	if (rc != SQLITE_OK) return 1;
	app_main(atoi(argv[1]));

	sqlite3_close(db);

	debug << "exit\n";
	return 0;
}

void assert_dir()
{
	if (mkdir(PUBK_DIR, 0777) < 0 && errno != EEXIST)	{
		// shall not happen 
		throw new SyscallException();
	}
}

void add_pubk_file(const char* filename, const char* content, int cont_len) 
{
	assert_dir();
	// of course, pubk_dir is either absolute or in my directory
	string path = PUBK_DIR;
	path += "/";
	path += filename;

	// I use only pem keys
	path += ".pem";

	int fd = open(path.c_str(), O_CREAT | O_WRONLY | O_EXCL, 0644);
	if (fd < 0)	{
		if (errno == EEXIST)	{
			// it's NOT a syscall exception, but wtever
			throw new SyscallException();
		}
		else	{
			throw new SyscallException();
		}
	}
	if (write(fd, content, cont_len) < 0)	{
		throw new SyscallException();
	}

	if (close(fd) < 0)	{
		throw new SyscallException();
	}
}
void app_main(int socknum)
{
	int sock = gen_sock(socknum);

	vec = new vector<pthread_t>;

	pthread_mutex_init(&vec_mtx, NULL);
	pthread_mutex_init(&db_mtx, NULL);
	pthread_t looper_t;
	pthread_create(&looper_t, NULL, &looper, &sock);
	pause();
	pthread_mutex_destroy(&vec_mtx);
	pthread_mutex_destroy(&db_mtx);
	close(sock);
	delete vec;

}
void* looper(void* sockv)
{
	int sock = *((int *)sockv);
	sockaddr_in client;
	socklen_t cl_len = sizeof client;

	int cl_sock;
	while (!signal_sent)	
	{
		cl_sock = accept(sock, (struct sockaddr *) &client, &cl_len);
		if (cl_sock < 0)	{
			perror("error accepting socket");
			exit(1);
		}
		cout << "Accepted. ip: " << client.sin_addr.s_addr << "\tport: " << client.sin_port << "\n";


		ThreadParams* tp = new ThreadParams;

		tp->idx = vec->size();
		vec->push_back(0);
		tp->cl_sock = cl_sock;
		if (pthread_mutex_lock(&vec_mtx) < 0) exit_error("lock mutex mt");
		if (pthread_create(&vec->at(tp->idx), NULL, &forward, tp) < 0) exit_error("pthread start");
		if (pthread_mutex_unlock(&vec_mtx) < 0) exit_error("unlock mutex mt");
	}
}

void read_sock(char* buffer, int bytno, int sock)	{
	int n = 0;

	do
	{
		int read_now;
		if ((read_now = read(sock, buffer + n, bytno - n)) < 0)	{
			perror("read");
			terminate();
		}
		n += read_now;
	} while ((bytno - n) > 0);
}
void* forward(void* tpv)
{

	ThreadParams* tp = (ThreadParams*) tpv;
	int cl_sock = tp->cl_sock;

	short code = ShortServerData::get_short(cl_sock);
	switch(code)	{
	case LOGIN:
		check_login(cl_sock);
		break;
	case ADD_ROUTER:
		add_router_sock(cl_sock);
		break;
	case GET_ROUTERS:
		get_routers_admin(db, cl_sock);
		break;
	case ADD_ADMIN:
		add_admin(cl_sock);
		break;
	case REMOVE_ROUTER:
		remove_router(cl_sock);
		break;
	case GET_ROUTERS_CLIENT:
		get_routers_client(db, cl_sock);
		break;
	case GET_ROUTER_PK:
		get_router_pubk(cl_sock);
		break;
	default:
		debug << code << " " << "no such code! " << "\n";
		// error
	}
	close(cl_sock);

	if (pthread_mutex_lock(&vec_mtx) < 0)
		exit_error("pthread_mute_lock");
	vec->erase(vec->begin() + tp->idx);
	if (pthread_mutex_unlock(&vec_mtx) < 0)	{
		perror("Can't unlock mutex");
		exit(1);	// Exits all threads because this mutex is locked forever
	}
	delete tp;

	return NULL;
}

string read_entire_file(const char* filename)
{
	int fd = open(filename, O_RDONLY);
	if (fd < 0)	{
		throw new SyscallException();
	}
	int len_read = 1;
	char buff[MAX_SIZE + 1];
	string output;
	do	{
		len_read = read(fd, buff, MAX_SIZE);
		if (len_read < 0) throw SyscallException();
		output += buff;
	} while (len_read != 0);
	if (close(fd) < 0) throw SyscallException();
	return output;

}
string _get_router_pubk(char* filename)
{
	string path = PUBK_DIR;
	path += "/";
	path += filename;
	string out;
	try	{
		out = read_entire_file(path.c_str());
	}	catch (Exception* e)	{
		debug << e->error_cause() << " ";
		return string("");
	}
	return out;
}
void get_router_pubk(int cl_sock)
{
	int ip = IntegerServerData::get_int(cl_sock);
	debug << "ip: " << ip << "\n";
	short port = ShortServerData::get_short(cl_sock);
	debug << "port: " << port << "\n";
	char fname[20];
	// TODO: I might want to support other
	// formats in the future
	sprintf(fname, "%d%d.pem", ip, port);
	string cont = _get_router_pubk(fname);
	ServerData* data;
	try	{
		data = new StringServerData(cont.c_str(), cont.size(), false);
	}	catch (Exception* e)	{
		debug << e->error_cause() << " ";
		data = new StringServerData("");
	}
	send_server(cl_sock, &data, 1);
	delete data;

}
int get_creds(char** user, char** pass, int sock)
{
	// user and pass are pointers to null, or at least
	// not used
	FBConv conv;

	read_sock(conv.bin, 4, sock);
	if (conv.val > SIZE) {
		debug << "Invalid string length: " << conv.val << "\n";
		terminate();
	}

	*user = new char[conv.val + 1];
	(*user)[conv.val] = 0;

	read_sock(*user, conv.val, sock);
	read_sock(conv.bin, 4, sock);

	*pass = new char[conv.val + 1];
	(*pass)[conv.val] = 0;

	read_sock(*pass, conv.val, sock);

	return 0;
}

void add_router_sock(int cl_sock)
{
	// I need to receive ip (coded for sake of simplicity), port (int),
	// and username-password pair 
	FBConv conv;

	char *username, *password;
	get_creds(&username, &password, cl_sock);
	int code = check_user_login(username, password);

	debug << username << ", " << password << "\n";
	int status = 0;
	delete password;	// security reasons
	if (code == AUTH_OK)	{
		int ipv4 = IntegerServerData::get_int(cl_sock);
		int port = IntegerServerData::get_int(cl_sock);

		debug << port << ", " << ipv4 << "\n";

		// TODO fix
		int len = IntegerServerData::get_int(cl_sock);

		if (len <= 0)	{
			throw BadInputException();
		}
		char* pubk_content = new char[len + 1];
		if (read(cl_sock, pubk_content, len) < 0)	{
			throw new SyscallException();
		}

		char filename[22];
		sprintf(filename, "%d%d", ipv4, port);
		add_pubk_file(filename, pubk_content, len);
		delete pubk_content;

		status = add_router(ipv4, port, username);
	}

	ServerData* data = NULL;
	if (status)	{
		// TODO Magic numbers
		data = new ShortServerData(1);
	}	else	{
		data = new ShortServerData(2);
	}
	send_server(cl_sock, &data, 1);
	delete username, data;

}

void check_login(int cl_sock)
{
	char *username, *password;

	get_creds(&username, &password, cl_sock);
	int exit_code = check_user_login(username, password);
	
	ServerData* data = new ShortServerData(exit_code % 256);

	// output the exit code to socket
	send_server(cl_sock, &data, 1);

	delete username, password, data;
}
void hex_dump(void* h_str, int len)
{
	for (int i = 0; i < len; ++i)
		printf("%x ", ((unsigned char *)h_str)[i]);
	cout << endl;
}

static int router_callback(void* sockv, int argc, char **argv, char** colname)
{
	// data is returned as:
	// ip (int), port(int), add_by(string), add_date (string), last_ver (string)
	
	int sock = *((int *)sockv);
	ServerData** data = new ServerData*[argc];
	for (int i = 0; i < 2; ++i)	{
		data[i] = new IntegerServerData(atoi(argv[i]));
	}
	for (int i = 2; i < argc; ++i)	{
		data[i] = new StringServerData(argv[i]);
	}
	
	send_server(sock, data, argc, true);
	for (int i = 0; i < argc; ++i) delete data[i];
	delete data;
	
	return 0;
}

void compute_hash_salt(unsigned char* str, int stlen, unsigned char* salt, unsigned char* hash, int times)
{
	if (times < 2) salt = NULL;
	SHA256(str, stlen, hash);
	
	for (int i = 0; i < HASH_LEN; ++i)	{
		hash[i] ^= salt[i];
	}
	for (int i = 1; i < times; ++i)	{
		SHA256(hash, 32, hash);
	}

}

int check_user_login(char* username, char* password)
{
	user_info *uinfo = new user_info;
	unsigned char hash[HASH_LEN];
	uinfo->set_username((unsigned char *) username);
	if (get_user(db, uinfo) != 0)	{
		cout << "no such user\n";
		return USER_NEX;
	}

	compute_hash_salt((unsigned char *) password, strlen(password), uinfo->salt, hash);

	for (int i = 0; i < HASH_LEN; ++i)	{
		if (hash[i] != uinfo->hash[i])
			return WRNG_PAS;
	}
	return AUTH_OK;
}

int _add_to_table(char* username, char* hash, char* salt)
{
	sqlite3_stmt* stmt;
	const char* szErrorMsg;

	const char* insert_stmt = "INSERT INTO admins(username, hash, salt) VALUES(?, ?, ?)";
	int rc = sqlite3_prepare(db, insert_stmt, strlen(insert_stmt), &stmt, &szErrorMsg);
	if (rc == SQLITE_OK)
	{
		sqlite3_bind_text(stmt, 1, username, strlen(username), 0);
		sqlite3_bind_text(stmt, 2, hash, HASH_LEN, 0);
		sqlite3_bind_text(stmt, 3, salt, HASH_LEN, 0);

		sqlite3_step(stmt);
		sqlite3_finalize(stmt);
	}
	else return -1;


	return 0;
}
int add_router(int ipv4, int port, char* adder)
{
	// I made it static so it wont be regenerated each call
	// it's length is 64, which is good
	// of course, I made it so that the id will be in a human readable format
	static const int src_len = 64;
	static const char src_chr[] = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm0987654321-_";
	// TODO change to something more economical
	int seed_len = ID_LEN;
	char *id = new char[ID_LEN];
	// char is of range 256, my source is only 64 options
	unsigned char seed[seed_len];
	const int MAXLEN = 80;
	char *s = new char[MAXLEN];
	time_t t = time(0);
	strftime(s, MAXLEN, "%d.%m.%Y", localtime(&t));

	int rc = RAND_bytes(seed, seed_len);
	if (!rc) exit_error("rand bytes gen");

	for (int i = 0; i < seed_len; ++i)	{
		// Since 64|256 the distribution is still uniform
		id[i] = src_chr[seed[i] % src_len];;
	}

	// I have an id but I don't currently use it
	int exit_status = _add_router(ipv4, port, s, adder, s);

	delete id, s;

	return exit_status;

}
int _add_router(int ipv4, int port, char* date_added, char* adder, char* last_ver)
{
	sqlite3_stmt* stmt;
	const char* szErrorMsg;

	// I generated some of the values, so I don't have to
	// bind all of them
	const char* insert_stmt = "INSERT INTO routers(ip, port, add_by, add_date, last_ver) VALUES(?, ?, ?, ?, ?)";

	int rc = sqlite3_prepare(db, insert_stmt, strlen(insert_stmt), &stmt, &szErrorMsg);
	if (rc == SQLITE_OK)
	{
		sqlite3_bind_int(stmt, 1, ipv4);
		sqlite3_bind_int(stmt, 2, port);
		sqlite3_bind_text(stmt, 3, adder, strlen(adder), 0);
		sqlite3_bind_text(stmt, 4, date_added, strlen(date_added), 0);
		sqlite3_bind_text(stmt, 5, last_ver, strlen(last_ver), 0);

		if (pthread_mutex_lock(&db_mtx) < 0) exit_error("mutex lock");
		rc = sqlite3_step(stmt);
		if (pthread_mutex_unlock(&db_mtx) < 0) {
			perror("unlocking mutex db");
			exit(1);
		}

		sqlite3_finalize(stmt);
	}
	else return 0;

	if (rc != SQLITE_DONE)	{
		return 0;
	}

	return 1;
}

int add_admin(int sock)
{
	bool err = true;
	char *username, *password;
	get_creds(&username, &password, sock);
	int exit_code = check_user_login(username, password);
	if (exit_code == AUTH_OK)	{
		err = false;
		delete username, password;
		username = password = NULL;
		get_creds(&username, &password, sock);
		if (add_to_table(username, password) < 0)	err = true;
		delete username, password;
	}

	ServerData* data = NULL;
	if (err)	{
		data = new ShortServerData(2);
	}	else	{
		data = new ShortServerData(1);
	}

	send_server(sock, &data, 1);
	delete data;
}
int add_to_table(char* username, char* password)
{
	unsigned char hash[HASH_LEN];
	unsigned char salt[HASH_LEN];
	
	int rc = RAND_bytes(salt, HASH_LEN);
	if (!rc) exit_error("rand bytes gen");

	compute_hash_salt((unsigned char *) password, strlen(password), salt, hash);
	if (_add_to_table(username, (char*) hash, (char*) salt) < 0) return -1;

	return 0;
}

void remove_router(int sock)
{
	// TODO change ports to 2 bytes instead of
	// wasteful 4 bytes
	char *username, *password;
	ServerData* data = NULL;
	FBConv ip, port;
	get_creds(&username, &password, sock);
	int exit_code = check_user_login(username, password);
	if (exit_code == AUTH_OK)	{
		read_min(sock, ip.bin, 4);
		read_min(sock, port.bin, 4);
		if (_remove_router(ip.val, port.val))	{
			// TODO MN
			data = new ShortServerData(1);
		}
	}
	if (data == NULL)	{
		data = new ShortServerData(2);
	}
	send_server(sock, &data, 1);
	delete data;
}
int _remove_router(int ipv4, int port)
{
	sqlite3_stmt* stmt;
	const char* szErrorMsg;
	// Very weird bug changing values after printing them
	unsigned char* temp;
	const char* select_stmt = "DELETE FROM routers WHERE ip = ? AND port = ?";
	int rc = sqlite3_prepare(db, select_stmt, strlen(select_stmt), &stmt, &szErrorMsg);
	if (rc == SQLITE_OK)
	{
		sqlite3_bind_int(stmt, 1, ipv4);
		sqlite3_bind_int(stmt, 2, port);

		if (pthread_mutex_lock(&db_mtx) < 0) exit_error("mutex lock");
		rc = sqlite3_step(stmt);
		if (pthread_mutex_unlock(&db_mtx) < 0) {
			perror("unlocking mutex db");
			exit(1);
		}

		// TODO make work
		// if (row != SQLITE_OK) 	return row;
	
		sqlite3_finalize(stmt);
	}
	else return 0;

	return 1;
}

void _send_routers(sqlite3* db, const char* select_stmt, int output)
{
	ServerData* data = NULL;
	char* szErrorMsg;
	int* outputv = new int(output);
	
	sqlite3_exec(db, select_stmt, router_callback, outputv, &szErrorMsg);
	delete outputv;
	data = new IntegerServerData(0);
	send_server(output, &data, 1);
}
int get_routers_admin(sqlite3* db, int outputStream)
{
	char *name, *pass;
	get_creds(&name, &pass, outputStream);
	ServerData* data = NULL;
	if (check_user_login(name, pass) != AUTH_OK){
		// something that is *not* 0
		data = new IntegerServerData(-1);
		send_server(outputStream, &data, 1);
		delete data;
		return 1;
	}

	_send_routers(db, "SELECT * FROM routers", outputStream);
	return 0;
}

int get_routers_client(sqlite3* db, int outputStream)
{
	_send_routers(db, "SELECT ip, port, add_date, last_ver FROM routers", outputStream);
	return 0;
}
int get_user(sqlite3* db, user_info* inf)
{
	sqlite3_stmt* stmt;
	const char* szErrorMsg;
	// Very weird bug changing values after printing them
	unsigned char* temp;
	const char* select_stmt = "SELECT hash, salt FROM admins WHERE username = ?";
	int rc = sqlite3_prepare(db, select_stmt, strlen(select_stmt), &stmt, &szErrorMsg);
	if (rc == SQLITE_OK)
	{
		sqlite3_bind_text(stmt, 1, (char *) inf->username, strlen((char *)inf->username), 0);

		if (pthread_mutex_lock(&db_mtx) < 0) exit_error("mutex lock");
		int row = sqlite3_step(stmt);
		if (pthread_mutex_unlock(&db_mtx) < 0) {
			perror("unlocking mutex db");
			exit(1);
		}
		if (row != SQLITE_ROW) 	return row;
	
		inf->set_hash((unsigned char*) sqlite3_column_text(stmt, 0));
		inf->set_salt((unsigned char*) sqlite3_column_text(stmt, 1));

		sqlite3_finalize(stmt);
	}
	else
	{
		return rc;
	}

	return 0;
}


























#ifndef COMM_CONV_H
#define COMM_CONV_H

#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include <cstring>
#include <unistd.h>
#include <sys/types.h>

#include "MyParser.h"
#include "Exceptions.h"

#define SIZE 256
#define STR_LEN 4

class IntegerServerData;

class ServerData
{
public:
	virtual void send_data(int sock) = 0;
	virtual void* getVal() = 0;
	virtual int get_len() = 0;
	
};

class StringServerData : public ServerData
{
	bool del;
	const char* data;
	int len;
public:
	StringServerData(const char* data, const bool del = false)
	{
		if (data == NULL) throw new NullPointerException();
		this->data = data;
		this->del = del;
		this->len = strlen(data);
	}
	StringServerData(const char* data, const int len, bool del)
	{
		if (data == NULL) throw new NullPointerException();
		this->data = data;
		this->del = del;
		this->len = len;
	}
	~StringServerData()
	{
		if (del && data != NULL)	{
			delete data;
			data = NULL;
		}
	}
	void send_data(int sock)
	{
		FBConv conv;
		conv.val = len;
		if (write(sock, conv.bin, 4) < 0) throw new SyscallException();
		if (write(sock, data, len) < 0)	throw new SyscallException();
		
	}
	
	void* getVal(){return (void *)data;	}
	int get_len()	{return STR_LEN+len;	}
};


class IntegerServerData : public ServerData
{
	int data;
public:
	IntegerServerData(int data)
	{
		this->data = data;
	}
	void send_data(int sock)
	{
		FBConv conv;
		conv.val = data;
		if (write(sock, conv.bin, 4) < 0) throw new SyscallException();
		
	}

	static int get_int(int sock)
	{
		FBConv conv;
		if (read(sock, conv.bin, 4) < 0) throw new SyscallException();	
		return conv.val;
	}

	int get_len()	{return 4;	}
	void* getVal(){return new int(data);	}

};

class ShortServerData : public ServerData
{
	short data;
public:
	ShortServerData(short data)
	{
		this->data = data;
	}
	void send_data(int sock)
	{
		TBConv conv;
		conv.val = data;
		if (write(sock, conv.bin, 2) < 0) throw new SyscallException();
		
	}

	static short get_short(int sock)
	{
		TBConv conv;
		if (read(sock, conv.bin, 2) < 0) throw new SyscallException();	
		return conv.val;
	}

	int get_len()	{return 2;	}
	void* getVal(){return new short(data);	}

};

/**
* sock - the output socket
* data - an array of ServerData instances
* len - length of the array
* bool more_data - if I'm planning to send anything else
*/
void send_server(int, ServerData**, int, bool = false);

class ServerProtocol
{
public:
	virtual void recv(int sock) = 0;
};

class AddRouterProtocol : public ServerProtocol
{
	unsigned int ipv4, port;
	short len;
	char* pubk;
public:

	AddRouterProtocol()
	{
		pubk = NULL;
	}
	void recv(int sock);

	~AddRouterProtocol()
	{
		if (pubk)	{
			delete pubk;
		}
	}
};

class AuthenticateProtocol : public ServerProtocol
{
	char* user;
	char* pass;
public:

	AuthenticateProtocol()
	{
		user = NULL;
		pass = NULL;
	}

	~AuthenticateProtocol()
	{
		if (user) delete user;
		if (pass) delete pass;
	}

	void recv(int sock);

	char* get_pass()	{return pass;}
	char* get_user()	{return user;}

};

#endif





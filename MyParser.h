#ifndef MY_PARSER_H
#define MY_PARSER_H


#include <string>
#include <arpa/inet.h>
#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "util.h"

using namespace std;

union FBConv
{
	int val;
	char bin[4];
};

union TBConv
{
	int val;
	char bin[2];
};

/***********************************
*information packet structure - 
*dst ip(4), dst port(2), flags(2)
*data-size(4), data($)
***********************************/
// eache level peeling one encrpytion
class MyPacket
{
	char* data;
	TBConv data_len;
	union FBConv ip;	// 4 bytes
	union TBConv port;	// 2 bytes
	union TBConv flags;	// 2 bytes
	MyPacket(){}
public:
	static char* int_to_ip(FBConv ip)
	{
		char* out = new char[100];
		int vals[4];
		for (int i = 0; i < 4; ++i)	{
			vals[i] = (ip.bin[i] + 256) % 256;
		}
		sprintf(out, "%d.%d.%d.%d", vals[0], vals[1], vals[2], vals[3]);
		return out;
	}
	MyPacket(int sock)
	{
		// size of one packet can never exceed 2^16
		TBConv size;
		read_min(sock, ip.bin, 4);
		read_min(sock, port.bin, 2);
		read_min(sock, flags.bin, 2);
		read_min(sock, data_len.bin, 2);
		data = new char[data_len.val + 1];
		data[data_len.val] = 0;
		read_min(sock, data, data_len.val);

	}

	~MyPacket()
	{
		if (data) delete data;
	}

	void forward()
	{
		cout << port.val << ", " << int_to_ip(ip) << endl;
		TCPSocket* socket = new TCPSocket(int_to_ip(ip), port.val);
		socket->send(data, data_len.val);
	}
};


#endif

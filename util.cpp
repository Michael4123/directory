#include "util.h"

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>

#include <cstdlib>

#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <exception>
#include <cstring>

#include <pthread.h>

Debug debug;
using namespace std;


// binding to inaddr_any
int gen_sock(int port, int lis_max)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)	throw new SyscallException();
	
	struct sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof serv_addr);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	
	if (bind(sock, (struct sockaddr *) &serv_addr, sizeof serv_addr) < 0)	throw new SyscallException();
	if (listen(sock, lis_max) < 0) throw new SyscallException();	// no more than 5 pending sockets
	return sock;
}

void read_min(int sock, char* buffer, int bytno)	
{
	int n = 0;

	do
	{
		int read_now;
		if ((read_now = read(sock, buffer + n, bytno - n)) < 0)	{
			throw new SyscallException();
		}
		n += read_now;
	} while ((bytno - n) > 0);
}

char* read_min(int sock, int bytno)
{
	char* buffer = new char[bytno + 1];
	buffer[bytno] = 0;
	int n = 0;
	do
	{
		int read_now;
		if ((read_now = read(sock, buffer + n, bytno - n)) < 0)	{
			throw new SyscallException();
		}
		n += read_now;
	} while ((bytno - n) > 0);
	return buffer;
}



TCPSocket::TCPSocket(char* ip, int port)
{
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)	{
		throw new SyscallException();
	}

	struct sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof serv_addr);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = inet_addr(ip);
	serv_addr.sin_port = htons(port);
	
	if (connect(sock, (struct sockaddr *) &serv_addr, sizeof serv_addr) < 0)	{
		throw new SyscallException();
	}
	open = true;
}

TCPSocket::~TCPSocket()
{
	close(sock);
}

void TCPSocket::send(char* data, size_t data_len)
{
	if (!open)	{
		// TODO throw error
		return;
	}
	if (write(sock, data, data_len) < 0)	{
		throw new SyscallException();
	}
}




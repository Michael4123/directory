#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <exception>

#include <pthread.h>

#include "Exceptions.h"

// parameters : port to listen at and maximum number of pending clients
int gen_sock(int, int = 5);


/*
* parameters: socket to read from, buffer to read to
* and number of bytes to read
*/

void read_min(int, char*, int);

/*
* parameters: socket to read from and number
* of bytes to read
*/

char* read_min(int, int);

class Debug
{
public:
	template<class T> Debug operator<<(T t){std::cout << t; return *this; }
};

class TCPSocket
{
	int sock;
	bool open;
public:
	TCPSocket(char* ip, int port);

	~TCPSocket();

	void send(char* data, size_t data_len);
};

#endif
